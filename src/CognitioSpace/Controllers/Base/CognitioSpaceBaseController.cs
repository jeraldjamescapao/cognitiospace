using System.Collections.Generic;
using Framework.UI.MVC;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace CognitioSpace.Controllers.Base
{
    /// <summary>
    /// The CognitioSpace base controller class handling.
    /// </summary>
    public abstract class CognitioSpaceBaseController : BaseController
    {
        #region Ctor

        protected CognitioSpaceBaseController(IConfiguration config, ILogger<CognitioSpaceBaseController> logger, IMemoryCache memoryCache) 
            : base(config, logger, memoryCache) { }

        #endregion
        
        #region Overrides
        
        /// <summary>
        /// Standard member initialization routine.
        /// </summary>
        protected override void InitializeMembers()
        {
            base.InitializeMembers();
            SupportedCultures = new List<string>() { "en-US", "fr-CH", "de-CH", "it-CH" }; // ToDo: Supported Culture/Languages list for the site must come from a source.
        }
        
        #endregion
    }
}