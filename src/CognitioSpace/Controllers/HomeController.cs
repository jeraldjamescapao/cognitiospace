﻿using CognitioSpace.Controllers.Base;
using CognitioSpace.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Diagnostics;

namespace CognitioSpace.Controllers
{
    /// <summary>
    /// The Landing Page (Home page) controller class handling.
    /// </summary>
    public class HomeController : CognitioSpaceBaseController 
    {
        #region Ctor

        public HomeController(IConfiguration config, ILogger<HomeController> logger, IMemoryCache memoryCache) 
            : base(config, logger, memoryCache) {}

        #endregion
        
        #region Properties

        /// <summary>
        /// Gets the header title used to show in view.
        /// </summary>
        protected override string MainHeaderTitle => "CognitioSpace | Welcome";

        #endregion
        
        #region Actions
        
        /// <summary>
        /// Gets the index page view. => GET: Home/Index
        /// </summary>
        public IActionResult Index()
        {
            ViewBag.HeaderTitle = MainHeaderTitle;
            return SafeExecuteViewResult(View);
        }

        /// <summary>
        /// Gets the privacy page view. => GET: Home/Privacy
        /// </summary>
        public IActionResult Privacy()
        {
            return SafeExecuteViewResult(View);
        }

        /// <summary>
        /// Gets the error page view. => GET: Home/Error
        /// </summary>
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return SafeExecuteViewResult(() => 
                View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier}));
        }
        
        #endregion
    }
}